# Coding Dojo: gRPC
Pour en apprendre un peu plus sur gRPC: https://www.grpc.io/docs/what-is-grpc/introduction/

## Dépendance
* Python 3: https://www.python.org/downloads/
* Docker: https://docs.docker.com/engine/install/

## Environnement virtuel Python
L'environnement virtuel Python vous permettra de gérer localement au projet vos dépendances.  
Cela vous permettra entre autre de garder votre installation globale de Python propre et d'éviter des conflits de versions
avec d'autres projets sur certaines dépendances Python.

Pour plus d'informations:
* https://python.land/virtual-environments
* https://python.land/virtual-environments/virtualenv

### Création de l'environnement
Pour créer l'environnement et l'activer, executez les commandes suivantes
```
python -m venv ./venv
source ./venv/bin/activate
```

### Installation des dépendances
```
pip install -r requirements.txt
```

### Activation de l'environnement virtuel Python  
Activation de l'environnement:
```
source ./venv/bin/activate
```

Désactivation:
```
deactivate
```

## Generation du code gRPC
Si vous modifiez le fichier protobuf, il vous faudra regénérer le code de votre client et de votre serveur.  
Pour ce faire, vous pouvez utilisez le script suivant:
```
. ./generate_protofiles.sh
```

## Comment tester ?
### Docker
```
docker-compose up
```

### Manuellement
Dans un premier terminal:
```
python python/server/py-server/hello_server.py 
```

Dans un second terminal:
```
python python/client/py-client/hello_client.py
```
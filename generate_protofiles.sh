#!/bin/bash

fun generate_grpc_code () {
  local output_dir="./python/$1/py-$1"
  local proto_dir="./protos"

  python -m grpc_tools.protoc -I${proto_dir} --python_out=${output_dir} --grpc_python_out=${output_dir} ${proto_dir}/hello.proto
  echo "Generated gRPC code for $1 => output directory: ${output_dir}"
}

generate_grpc_code client
generate_grpc_code server
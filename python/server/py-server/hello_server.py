import logging
import os
from concurrent import futures

import grpc
import hello_pb2
import hello_pb2_grpc

logger = logging.getLogger('grpc_server')
logger.setLevel(logging.INFO)


def get_port():
    return os.getenv('PY_SERVER_PORT', '50051')


class Greeter(hello_pb2_grpc.GreeterServicer):
    def SayHello(self, request, context):
        logger.info('Received hello from {}'.format(request.name))
        return hello_pb2.HelloReply(message='Hello {} !'.format(request.name))


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    hello_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    port = get_port()
    server.add_insecure_port('[::]:{}'.format(port))
    server.start()
    logger.info('Server started and listening on port {}...'.format(port))
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()

import logging
import os

import grpc

import hello_pb2
import hello_pb2_grpc

logger = logging.getLogger('grpc_server')
logger.setLevel(logging.INFO)


def get_host():
    return os.getenv('PY_SERVER_HOST', 'localhost')


def get_port():
    return os.getenv('PY_SERVER_PORT', '50051')


def start_client():
    host = get_host()
    port = get_port()
    channel = grpc.insecure_channel('{}:{}'.format(host, port))
    stub = hello_pb2_grpc.GreeterStub(channel)
    logger.info('Sending request to server({}:{})...'.format(host, port))
    response = stub.SayHello(hello_pb2.HelloRequest(name='you'))
    logger.info('Received response : {}'.format(response.message))


if __name__ == '__main__':
    logging.basicConfig()
    logger.info('Starting client...')
    start_client()
